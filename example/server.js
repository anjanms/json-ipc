var jsonipc = require('..'),
    server = new jsonipc.Server(),
    logging = true;

server.on('open', function(path) {
    if (logging) console.log('Server Opened', path);
    server.broadcast({message: 'Server Started'});
});

server.on('error', function(err) {
    console.error('Server Error', err.stack);
});

server.on('connect', function(socket) {
    server.send(socket, {message: 'Hello World!'});
});

server.on('message', function(socket, data) {
    if (logging) console.log('Server Receive', data);

    data.message = 'Pong!';
    data.time = Date.now() - data.time;

    server.send(socket, data);
});

server.on('close', function(path) {
    if (logging) console.log('Server Closed', path);
});

server.open('example');
