var jsonipc = require('..'),
    client = new jsonipc.Client(),
    logging = true;

client.on('open', function(path) {
    if (logging) console.log('Socket Opened', path);
});

client.on('error', function(err) {
    if (client.isConnected()) console.error('Socket Error', err.stack);
});

client.on('message', function(data) {
    if (logging) console.log('Socket Receive', data);
});

client.on('close', function(path) {
    if (logging) console.log('Socket Closed', path);
});

client.open('example');

setInterval(function() {client.send({message: 'Ping', time: Date.now()});}, 500);
