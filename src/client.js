var os = require('os'),
    net = require('net'),
    util = require('util'),
    events = require('events');

function socketOpen() {
    clearTimeout(this._reconnectTimeout);

    if (!this._connected) {
        this._connected = true;
        this.emit('open', this._path);
    }
}

function socketError(err) {
    this.emit('error', err);
}

function socketSend(data, callback) {
    if (typeof data === 'string') data = JSON.stringify(JSON.parse(data)) + '\x00';
    else if (typeof data === 'object') data = JSON.stringify(data) + '\x00';
    else throw new Error('Message must be a valid JSON string or an object');

    this._socket.write(data, callback);
}

function socketReceive(data) {
    data = data.split('\x00');
    data.pop();

    for (var i = 0; i < data.length; i++) {
        try {
            data[i] = JSON.parse(data[i]);
            this.emit('message', data[i]);
        } catch(err) {
            this.emit('error', err);
        }
    }
}

function socketClose() {
    if (this._reconnect) {
        clearTimeout(this._reconnectTimeout);
        this._reconnectTimeout = setTimeout(this._socket.connect.bind(this._socket, this._path), this._reconnectDelay);
    }

    if (this._connected) {
        this._connected = false;
        this.emit('close', this._path);
    }
}

function IPCClient() {
    events.EventEmitter.call(this);

    this._path = null;

    this._socket = new net.Socket();

    this._connected = false;
    this._reconnect = true;
    this._reconnectDelay = 60000;
    this._reconnectTimeout = undefined;

    this._socket.setEncoding('utf8');

    this._socket.on('connect', socketOpen.bind(this));
    this._socket.on('data', socketReceive.bind(this));
    this._socket.on('error', socketError.bind(this));
    this._socket.on('close', socketClose.bind(this));
}

util.inherits(IPCClient, events.EventEmitter);

IPCClient.prototype.open = function(name) {
    if (this._path || !name) return this;

    if (os.platform() === 'win32') this._path = '\\\\.\\pipe\\' + name + '.sock';
    else this._path = (os.tmpdir().slice(-1) === '/' ? os.tmpdir() : os.tmpdir() + '/') + name + '.sock';

    this._reconnect = true;
    this._socket.connect(this._path);

    return this;
};

IPCClient.prototype.isConnected = function() {
    return this._connected;
};

IPCClient.prototype.send = function(data, callback) {
    if (!this._path || !data) return false;

    socketSend.apply(this, arguments);

    return true;
};

IPCClient.prototype.close = function() {
    if (!this._path) return this;

    this._reconnect = false;
    this._socket.end();

    return this;
};

module.exports = IPCClient;