exports.version = require('../package.json').version;

exports.Client = require('./client.js');
exports.Server = require('./server.js');