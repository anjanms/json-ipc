var fs = require('fs'),
    os = require('os'),
    net = require('net'),
    util = require('util'),
    events = require('events');

function serverOpen() {
    if (this._perm) fs.chmodSync(this._path, this._perm);
    this.emit('open', this._path);
}

function serverError(err) {
    var that = this;

    if (err.code === 'EADDRINUSE') {
        var socket = new net.Socket();

        socket.on('error', function(err) {
            if (err.code === 'ECONNREFUSED') fs.unlink(that._path, that._server.listen.bind(that._server, that._path));
            socket.destroy();
            socket = null;
            that = null;
        });

        socket.on('connect', function() {
            that.emit('error', err);
            socket.destroy();
            socket = null;
            that = null;
        });

        socket.connect(that._path);
        return;
    }

    this.emit('error', err);
}

function serverClose() {
    this.emit('close', this._path);
    this._path = null;
    this._perm = null;
}

function socketOpen(socket) {
    socket.setEncoding('utf8');

    socket.on('data', socketReceive.bind(this, socket));
    socket.on('error', socketError.bind(this, socket));
    socket.on('close', socketClose.bind(this, socket));

    this._sockets.push(socket);

    this.emit('connect', socket);
}

function socketError(socket, err) {
    this.emit('error', err);
}

function socketSend(socket, data, callback) {
    if (typeof data === 'string') data = JSON.stringify(JSON.parse(data)) + '\x00';
    else if (typeof data === 'object') data = JSON.stringify(data) + '\x00';
    else throw new Error('Message must be a valid JSON string or an object');

    socket.write(data, callback);
}

function socketReceive(socket, data) {
    data = data.split('\x00');
    data.pop();

    for (var i = 0; i < data.length; i++) {
        try {
            data[i] = JSON.parse(data[i]);
            this.emit('message', socket, data[i]);
        } catch(err) {
            this.emit('error', err);
        }
    }
}

function socketClose(socket) {
    for (var i = 0; i < this._sockets.length; i++) {
        if (this._sockets[i] === socket) {
            this._sockets.splice(i, 1);
            break;
        }
    }
}

function IPCServer() {
    events.EventEmitter.call(this);

    this._path = null;
    this._perm = null;

    this._server = new net.Server();
    this._sockets = [];

    this._server.on('listening', serverOpen.bind(this));
    this._server.on('error', serverError.bind(this));
    this._server.on('close', serverClose.bind(this));

    this._server.on('connection', socketOpen.bind(this));
}

util.inherits(IPCServer, events.EventEmitter);

IPCServer.prototype.open = function(name, perm) {
    if (this._path || !name) return this;

    if (os.platform() === 'win32') this._path = '\\\\.\\pipe\\' + name + '.sock';
    else {
        this._path = (os.tmpdir().slice(-1) === '/' ? os.tmpdir() : os.tmpdir() + '/') + name + '.sock';
        if (typeof perm === 'number') this._perm = perm;
    }

    this._server.listen(this._path);
    return this;
};

IPCServer.prototype.send = function(socket, data, callback) {
    if (!this._path || !socket || !data) return false;

    socketSend.apply(this, arguments);

    return true;
};

IPCServer.prototype.broadcast = function(data) {
    if (!this._path || !data) return false;

    for (var i = 0; i < this._sockets.length; i++) {
        socketSend.call(this, this._sockets[i], data);
    }

    return true;
};

IPCServer.prototype.close = function() {
    if (!this._path) return this;

    for (var i = 0; i < this._sockets.length; i++) {
        this._sockets[i].destroy();
    }

    this._server.close();

    return this;
};

module.exports = IPCServer;